package controller;

import java.util.ArrayList;
import java.util.List;


import model.*;
import view.*;
import strategy.*;
import view.ClientsFrame;
import view.SimulatorFrame;

public class SimulationManager implements Runnable {
	private int timeLimit = 1000;
	private int maxProcessingTime;
	private int minProcessingTime;
	private int minArrival;
	private int maxArrival;
	private int numberOfServers;
	private int currentTime = -1;
	private int numberOfClients;
	private boolean restart = false;
	private boolean stop = false;
	private SelectionPolicy selectionPolicy = SelectionPolicy.SHORTEST_TIME;
	private Scheduler scheduler;
	private SimulatorFrame frame;
	private List<Client> generatedClients;

	public SimulationManager() {
		frame = new SimulatorFrame();
		frame.addStartButtonListener(e -> {
			currentTime = 0;
			maxProcessingTime = Integer.parseInt(frame.getMaxService());
			minProcessingTime = Integer.parseInt(frame.getMinService());
			minArrival = Integer.parseInt(frame.getMinArrive());
			maxArrival = Integer.parseInt(frame.getMaxArrive());
			timeLimit = Integer.parseInt(frame.getSimulationTime());
			numberOfServers = Integer.parseInt(frame.getNrOfQueues());
			numberOfClients = Integer.parseInt(frame.getNumberOfClients());
			frame.setStart(false);
		});
		frame.addStopButtonListener(e -> {
			stop = true;
		});
	}

	public void generateRandomClients() {
		int arrival = 1;
		int processing = 0;
		for (int i = 1; i <= numberOfClients; i++) {
			processing = minProcessingTime + (int) (Math.random() * ((maxProcessingTime - minProcessingTime) + 1));
			generatedClients.add(new Client(i, arrival, processing));
			arrival += minArrival + (int) (Math.random() * ((maxArrival - minArrival) + 1));
		}
	}

	@Override
	public void run() {
		restart = false;
		while (!restart) {
			while (currentTime < 0) {
				System.out.println("");
			}
			int peakHour = 0;
			int peakCustomers = 0;
			int[] totalServingTime = new int[3];
			int[] totalClients = new int[3];
			int[] timeClients = new int[3];
			int[] timeEmpty = new int[3];
			scheduler = new Scheduler(numberOfServers, 30, selectionPolicy);
			generatedClients = new ArrayList<>();
			this.generateRandomClients();
			ClientsFrame clientFrame = new ClientsFrame();
			System.out.println("START");
			while (currentTime <= timeLimit && !stop) {
				frame.addLog("Current Time: " + currentTime);
				clientFrame.resetText();
				for (int i = 0; i < generatedClients.size(); i++) {
					clientFrame.addText("Client: " + generatedClients.get(i).getId() + " Arrival: "
							+ generatedClients.get(i).getArrival() + " Serving Time: "
							+ generatedClients.get(i).getServing() + " FinishTime: "
							+ generatedClients.get(i).getFinish() + '\n');
				}

				if (!generatedClients.isEmpty() && generatedClients.get(0).getArrival() == currentTime) {
					Client c = generatedClients.get(0);
					try {
						int id = scheduler.addClient(c);
						if (id < 3) {
							totalServingTime[id] += c.getServing();
							timeClients[id] += c.getFinish() - c.getArrival();
							totalClients[id]++;
						}
						frame.addLog("Client with id = " + c.getId() + " has been added to server + " + id);
						if (scheduler.getServers().get(id).getNumberOfClients() == 1) {
							frame.addLog("Client with id = " + c.getId() + " has reached the counter of server " + id);
						}
						generatedClients.remove(0);
					} catch (Exception e) {
						numberOfServers++;
						scheduler.addNewServer();
						frame.addLog("All queues full! New server created!");
						generatedClients.get(0).setArrival(1);
					}
				}
				Client[][] clientMatrix = new Client[numberOfServers][20];
				int[] nrClients = new int[numberOfServers];
				int[] waitingTime = new int[numberOfServers];
				int sum = 0;
				for (int i = 0; i < numberOfServers; i++) {
					Server srv = scheduler.getServers().get(i);
					if (i < 3 && srv.getNumberOfClients() == 0) {
						timeEmpty[i]++;
					}
					sum += srv.getNumberOfClients();
					clientMatrix[i] = srv.getClients();
					if (clientMatrix[i] != null && clientMatrix[i][0] != null
							&& clientMatrix[i][0].getFinish() == currentTime + 1) {
						frame.addLog("Client with id = " + clientMatrix[i][0].getId()
								+ " has left the store through server" + i);
						if (clientMatrix[i][1] != null) {
							frame.addLog("Client with id = " + clientMatrix[i][1].getId()
									+ " has reached the counter of server " + i);
						}
					}
					nrClients[i] = srv.getNumberOfClients();
					waitingTime[i] = srv.getWaitingPeriod();
				}
				if (peakCustomers < sum) {
					peakCustomers = sum;
					peakHour = currentTime;
				}
				frame.displayData(clientMatrix, numberOfServers, nrClients, currentTime, waitingTime);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				currentTime++;
			}

			for (int i = 0; i < numberOfServers; i++) {
				scheduler.getServers().get(i).closeServer();
			}
			
			if(!stop) {
			float[] averageTime = new float[3];
			float[] avgServiceTime = new float[3];
			for (int i = 0; i < 3; i++) {
				if (i < numberOfServers) {
					averageTime[i] = ((float) timeClients[i]) / totalClients[i];
					avgServiceTime[i] = ((float) totalClients[i]) / totalClients[i];
				} else
					break;
			}
			frame.getSuccesMessage(averageTime, avgServiceTime, totalClients, timeEmpty, peakHour, peakCustomers);
			} else {
				clientFrame.dispose();
				frame.getErrorMessage("Simulation stopped");
			}
			frame.setStart(true);
			currentTime = -1;
			stop = false;
		}

	}

	public static void main(String[] args) {
		SimulationManager man = new SimulationManager();
		Thread t = new Thread(man);
		t.start();
	}

}
