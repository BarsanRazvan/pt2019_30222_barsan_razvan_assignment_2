package model;

public class Client {
	private int id;
	private int arrivalTime;
	private int finishTime;
	private int servingTime;
	
	public Client(int idp, int arrTime, int servTime){
		id = idp;
		arrivalTime = arrTime;
		finishTime = -1; 
		servingTime = servTime;
	}
	
	public int getId(){
		return id;
	}
	
	public int getArrival(){
		return arrivalTime;
	}
	
	public int getFinish(){
		return finishTime;
	}
	
	public int getServing(){
		return servingTime;
	}
	
	public void setFinish(int waitingPeriod){
		finishTime = arrivalTime + servingTime + waitingPeriod;
	}
	
	public void setArrival(int arrPeriod){
		arrivalTime += arrPeriod;
	}
	
}
