package model;

import java.util.ArrayList;
import java.util.List;
import strategy.*;

public class Scheduler {
	private List<Server> servers;
	private int maxNoOfServers;
	private int maxNoOfClientsPerServer;
	private Strategy strategy;

	public Scheduler(int maxNoOfServers, int maxNoOfClientsPerServer, SelectionPolicy policy) {
		Server srv;
		this.maxNoOfServers = maxNoOfServers;
		this.maxNoOfClientsPerServer = maxNoOfClientsPerServer;
		servers = new ArrayList<>();
		for (int i = 0; i < maxNoOfServers; i++) {
			srv = new Server();
			Thread t = new Thread(srv);
			t.start();
			servers.add(srv);
		}
		changeStrategy(policy);
	}

	public void changeStrategy(SelectionPolicy policy) {
		if (policy == SelectionPolicy.SHORTEST_QUEUE)
			strategy = new ConcreteStrategyQueue();
		if (policy == SelectionPolicy.SHORTEST_TIME)
			strategy = new ConcreteStrategyTime();
	}

	public List<Server> getServers() {
		return servers;
	}

	public void stopThreads() {
		for (int i = 0; i < maxNoOfServers; i++) {
			servers.get(i).closeServer();
		}
	}

	public void addNewServer() {
		Server srv = new Server();
		;
		this.maxNoOfServers++;
		Thread t = new Thread(srv);
		t.start();
		servers.add(maxNoOfServers - 1, srv);
	}

	public int addClient(Client c) throws Exception {
		return strategy.addClient(servers, c);
	}

}
