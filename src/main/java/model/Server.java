package model;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable {
	private BlockingQueue<Client> clients;
	private AtomicInteger waitingPeriod;
	private boolean shutdown;

	public Server() {
		clients = new ArrayBlockingQueue<>(30);
		waitingPeriod = new AtomicInteger(0);
		shutdown = false;
	}

	public void addClient(Client c) throws Exception {
		System.out.println("Client with id: " + c.getId() + " has been added");
		c.setFinish(waitingPeriod.get());
		if (clients.size() == 10)
			throw new Exception();
		clients.add(c);
		waitingPeriod.addAndGet(c.getServing());
	}

	public int getWaitingPeriod() {
		return waitingPeriod.get();
	}

	public int getNumberOfClients() {
		return clients.size();
	}

	public Client[] getClients() {
		return clients.toArray(new Client[10]);
	}

	public void closeServer() {
		shutdown = true;
	}

	@Override
	public void run() {
		while (!shutdown) {
			Client c = null;
			c = clients.peek();
			if (c != null) {
				System.out.println("Client with id: " + c.getId() + " has reached the counter");
				int time = c.getServing();
				for (int i = 0; i < time; i++) {
					if(shutdown)
						break;
					try {
						Thread.sleep(1000);
						waitingPeriod.decrementAndGet();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				clients.remove(c);
				System.out.println("Client with id: " + c.getId() + "has left the store");
				System.out.println("New waiting time " + waitingPeriod);
			}
		}
		System.out.println("Thread shutdown!");
	}

}
