package strategy;

import java.util.List;
import model.*;

public class ConcreteStrategyQueue implements Strategy {
	public int addClient(List<Server> servers, Client c) throws Exception {
		int minimumClientNumber = 1000;
		int poz = -1;
		for (Server i : servers) {
			int serverLength = i.getClients().length;
			if (serverLength < minimumClientNumber) {
				minimumClientNumber = serverLength;
				poz = servers.indexOf(i);
			}
		}

		servers.get(poz).addClient(c);
		return poz;
	}
}
