package strategy;

import java.util.List;
import model.*;

public class ConcreteStrategyTime implements Strategy {
	public int addClient(List<Server> servers, Client c) throws Exception {
		int minimumTime = 10000;
		int poz = -1;
		for (Server i : servers) {
			int waitingTime = i.getWaitingPeriod();
			if (waitingTime < minimumTime) {
				minimumTime = waitingTime;
				poz = servers.indexOf(i);
			}
		}

		servers.get(poz).addClient(c);
		return poz;
	}
}
