package strategy;

import java.util.List;
import model.*;

public interface Strategy {
	public int addClient(List<Server> servers, Client c) throws Exception;
}
