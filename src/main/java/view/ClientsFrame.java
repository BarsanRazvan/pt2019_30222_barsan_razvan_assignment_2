package view;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class ClientsFrame extends JFrame {
	private JTextArea clientsPanel;
	private JScrollPane scrollPanel;

	public ClientsFrame() {
		clientsPanel = new JTextArea(100, 100);
		scrollPanel = new JScrollPane(clientsPanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		this.add(scrollPanel);
		this.setSize(400, 400);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public void addText(String s) {
		clientsPanel.append(s + '\n');
	}

	public void resetText() {
		clientsPanel.setText("");
	}
}
