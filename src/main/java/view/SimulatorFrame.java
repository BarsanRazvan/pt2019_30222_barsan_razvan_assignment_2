package view;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import model.*;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class SimulatorFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private JScrollPane scrollPanel;
	private JScrollPane loggerPanel;
	private JPanel queuePanel;
	private JPanel inputPanel;
	private JPanel logAndInputPanel;
	private JButton startButton;
	private JButton stopButton;
	private JTextArea logger;
	private JTextField minArrive;
	private JTextField maxArrive;
	private JTextField minService;
	private JTextField maxService;
	private JTextField nrOfQueues;
	private JTextField simulationTime;
	private JTextField nrOfClients;
	private JLabel nrOfClientsLabel;
	private JLabel minArriveLabel;
	private JLabel maxArriveLabel;
	private JLabel minServiceLabel;
	private JLabel maxServiceLabel;
	private JLabel nrOfQueuesLabel;
	private JLabel simulationTimeLabel;
	private JLabel timeLabel;
	private JLabel realTimeLabel;
	private static final int WIDTH = 980;
	private static final int HEIGHT = 500;

	public SimulatorFrame() {
		createComponents();
		addComponents();
	}

	private void createComponents() {
		logger = new JTextArea(100, 100);
		logAndInputPanel = new JPanel();
		queuePanel = new JPanel();
		scrollPanel = new JScrollPane(queuePanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		loggerPanel = new JScrollPane(logger, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		inputPanel = new JPanel();
		startButton = new JButton("start");
		stopButton = new JButton("stop");
		minArrive = new JTextField();
		maxArrive = new JTextField();
		minService = new JTextField();
		maxService = new JTextField();
		nrOfQueues = new JTextField();
		nrOfClients = new JTextField();
		simulationTime = new JTextField();
		nrOfClientsLabel = new JLabel("Nr Of Clients:");
		minArriveLabel = new JLabel("Min Arrival:");
		maxArriveLabel = new JLabel("Max Arrival:");
		minServiceLabel = new JLabel("Min Service:");
		maxServiceLabel = new JLabel("Max Service:");
		nrOfQueuesLabel = new JLabel("Nr. of queues:");
		simulationTimeLabel = new JLabel("SimulationTime: ");
		timeLabel = new JLabel("Time: ");
		realTimeLabel = new JLabel("0s");
	}

	private void addComponents() {
		logger.setPreferredSize(new Dimension(80, 80));
		logAndInputPanel.setPreferredSize(new Dimension(300, 500));
		logAndInputPanel.setMaximumSize(new Dimension(300, 1000));
		inputPanel.setPreferredSize(new Dimension(80, 50));
		logger.setText("Logger:");
		test();
		inputPanel.setLayout(new GridLayout(9, 2));
		inputPanel.add(minArriveLabel);
		inputPanel.add(minArrive);
		inputPanel.add(maxArriveLabel);
		inputPanel.add(maxArrive);
		inputPanel.add(minServiceLabel);
		inputPanel.add(minService);
		inputPanel.add(maxServiceLabel);
		inputPanel.add(maxService);
		inputPanel.add(nrOfQueuesLabel);
		inputPanel.add(nrOfQueues);
		inputPanel.add(simulationTimeLabel);
		inputPanel.add(simulationTime);
		inputPanel.add(nrOfClientsLabel);
		inputPanel.add(nrOfClients);
		inputPanel.add(timeLabel);
		inputPanel.add(realTimeLabel);
		inputPanel.add(startButton);
		inputPanel.add(stopButton);
		logAndInputPanel.setLayout(new BoxLayout(logAndInputPanel, BoxLayout.Y_AXIS));
		logAndInputPanel.add(loggerPanel);
		logAndInputPanel.add(inputPanel);
		this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.X_AXIS));
		this.add(scrollPanel);
		this.add(logAndInputPanel);
		this.setSize(WIDTH, HEIGHT);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public void displayData(Client[][] clients, int nrOfServers, int[] nrOfClients, int time, int waitingTime[]) {
		queuePanel.removeAll();
		queuePanel.revalidate();
		queuePanel.repaint();
		queuePanel.setLayout(new BoxLayout(queuePanel, BoxLayout.X_AXIS));
		// queuePanel.setPreferredSize( new Dimension (220 , 220));
		realTimeLabel.setText(time + " s");
		for (int i = 0; i < nrOfServers; i++) {
			JPanel panel = new JPanel();
			JPanel labelsPanel = new JPanel();
			JTextArea area = new JTextArea(10, 30);
			panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
			panel.setPreferredSize(new Dimension(280, 500));
			panel.setMaximumSize(new Dimension(280, 500));
			JLabel srvLabel = new JLabel("Server" + i);
			JLabel waitLabel = new JLabel("WaitingTime: " + waitingTime[i] + "s");
			labelsPanel.setLayout(new GridLayout(2, 1));
			labelsPanel.add(srvLabel);
			labelsPanel.add(waitLabel);
			panel.add(labelsPanel);
			area.setMinimumSize(new Dimension(400, 500));
			area.setPreferredSize(new Dimension(400, 500));
			area.setMaximumSize(new Dimension(400, 500));
			panel.add(area);
			if (nrOfClients[i] != 0) {
				area.setText("");
				for (int j = 0; j < nrOfClients[i]; j++) {
					area.append("Id: " + clients[i][j].getId() + " Arrival: " + clients[i][j].getArrival()
							+ " Serving Time: " + clients[i][j].getServing() + " FinishTime: "
							+ clients[i][j].getFinish() + '\n');
				}
			} else
				area.setText("Empty");
			queuePanel.add(panel);
			queuePanel.add(Box.createRigidArea(new Dimension(5, 0)));
		}
	}

	public void addLog(String s) {
		logger.append('\n' + s);
	}

	public void addStartButtonListener(ActionListener ac) {
		startButton.addActionListener(ac);
	}

	public void addStopButtonListener(ActionListener ac) {
		stopButton.addActionListener(ac);
	}

	public String getMinArrive() {
		return minArrive.getText();
	}

	public String getMaxArrive() {
		return maxArrive.getText();
	}

	public String getMinService() {
		return minService.getText();
	}

	public String getMaxService() {
		return maxService.getText();
	}

	public String getNrOfQueues() {
		return nrOfQueues.getText();
	}

	public String getSimulationTime() {
		return simulationTime.getText();
	}

	public String getNumberOfClients() {
		return nrOfClients.getText();
	}

	public void setStart(boolean start) {
		startButton.setEnabled(start);
	}

	public void getSuccesMessage(float[] averageTime, float[] avgServiceTime, int[] totalClients, int[] timeEmpty,
			int peakHour, int peakCustomers) {
		String avgTime = "Avg time for queues - q0: " + averageTime[0] + ", q1: " + averageTime[1] + "q2: "
				+ averageTime[2];
		String avgService = "\nAvg service time for queues - q0: " + avgServiceTime[0] + ", q1: " + avgServiceTime[1]
				+ "q2: " + avgServiceTime[2];
		String total = "\nTotal Clients for queues - q0: " + totalClients[0] + ", q1: " + totalClients[1] + "q2: "
				+ totalClients[2];
		String empty = "\nEmpty time for queues - q0: " + timeEmpty[0] + ", q1: " + timeEmpty[1] + "q2: "
				+ timeEmpty[2];
		String peak = "\nPeak hour was " + peakHour + ", with a customer number of " + peakCustomers;
		JOptionPane.showMessageDialog(this, avgTime + avgService + total + empty + peak, "Simulation Completed!",
				JOptionPane.INFORMATION_MESSAGE);
	}

	public void getErrorMessage(String message) {
		JOptionPane.showMessageDialog(this, message, "Simulation canceled", JOptionPane.ERROR_MESSAGE);
	}

	public void test() {
		minArrive.setText("1");
		maxArrive.setText("2");
		minService.setText("15");
		maxService.setText("23");
		nrOfQueues.setText("1");
		nrOfClients.setText("100");
		simulationTime.setText("100");

	}
}
